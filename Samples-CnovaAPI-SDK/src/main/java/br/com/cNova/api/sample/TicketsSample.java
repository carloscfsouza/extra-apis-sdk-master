package br.com.cNova.api.sample;

import java.io.File;
import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.tickets.TicketsLoad;
import br.com.cNova.api.pojo.v2.tickets.messages.Messages;
import br.com.cNova.api.pojo.v2.tickets.messages.MessagesLoad;
import br.com.cNova.api.pojo.v2.tickets.messages.User;
import br.com.cNova.api.resources.v2.tickets.Tickets;
import br.com.cNova.api.resources.v2.tickets.TicketsResource;
import br.com.cNova.api.sample.utils.JSONPrinterUtil;
import br.com.cNova.api.sample.utils.Tokens;

public class TicketsSample {

	private TicketsResource api = new Tickets(Hosts.SANDBOX, Tokens.client_id,
			Tokens.access_token);
	
	private List<br.com.cNova.api.pojo.v2.tickets.Tickets> getTickets(Integer offset, Integer limit,
			String code, String status, String site, String createdAt, String closedAt, String attributes) 
			throws ServiceException {
		return api.getTickets(offset, limit, code, status, site, createdAt, closedAt, attributes);
	}
	
	private void postTickets() throws ServiceException {
		TicketsLoad ticketsLoad = new TicketsLoad();
		File loads = new File("C:\\Users\\cfarias\\workspace\\Samples-CnovaAPI-SDK-v2\\Samples-CnovaAPI-SDK-v2\\Tickets_JSON_V2.json");

		ticketsLoad.setJsonFile(loads);
		System.out.println("Ticket: "
				+ JSONPrinterUtil.print(api.postTickets(ticketsLoad)));
	}
 	
	private List<Messages> getListMessages(String code, Integer offset, Integer limit, String attributes)
			throws ServiceException {
		return api.getListMessages(code, offset, limit, attributes);
	}
	
	private void postTicketsMessages(String code) throws ServiceException {
		MessagesLoad messagesLoad = new MessagesLoad();
		File loads = new File("C:\\Users\\cfarias\\workspace\\Samples-CnovaAPI-SDK-v2\\Samples-CnovaAPI-SDK-v2\\Tickets_Messages_JSON_V2.json");

		messagesLoad.setJsonFile(loads);
		System.out.println("Tickets Messages: "
				+ JSONPrinterUtil.print(api.postTicketsMessages(code, messagesLoad)));
	}
	
	private void putTicketsAssignee(String code, String login, String name,
			String visibility, String body)
			throws ServiceException {

		User user = new User();
		user.setLogin(login);
		user.setName(name);
		
		Messages messages = new Messages();
		messages.setUser(user);
		messages.setVisibility(visibility);
		messages.setBody(body);
		
		System.out.println("PUT Tickets Assignee: "
				+ JSONPrinterUtil.print(api.putTicketsAssignee(code, messages)));
		
	}
	
	private void putTicketsAssignee(String code, String status)
			throws ServiceException {
		
		System.out.println("PUT Tickets Status: "
				+ JSONPrinterUtil.print(api.putTicketsStatus(code, status)));
		
	}
	
	
	public static void main(String[] args) {
		try {

			Integer offset = 0;
			Integer limit = 50;
			String code = "409921220457";
			String status = "";
			String site = "";
			String createdAt = "";
			String closedAt = "";
			String attributes = "";
			
			TicketsSample ticketsSample = new TicketsSample();
			
			/*List<br.com.cNova.api.pojo.v2.tickets.Tickets> list = 
					ticketsSample.getTickets(offset, limit, code, status, site, createdAt, closedAt, attributes);
			
			if (list != null && list.size() > 0) {
				for (br.com.cNova.api.pojo.v2.tickets.Tickets tickets : list) {
					System.out.println("================ TICKETS ===================");
					System.out.println(JSONPrinterUtil.print(tickets));
					System.out.println("================ TICKETS ===================");
				}
			}*/
			
			List<Messages> listM = ticketsSample.getListMessages(code, offset, limit, attributes);
			
			if (listM != null && listM.size() > 0) {
				for (Messages message : listM) {
					System.out.println("================ MESSAGE ===================");
					System.out.println(JSONPrinterUtil.print(message));
					System.out.println("================ MESSAGE ===================");
				}
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}
}
