package br.com.cNova.api.sample;

import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.sites.Sites;
import br.com.cNova.api.resources.v2.sites.SitesResource;
import br.com.cNova.api.sample.utils.Tokens;

public class SitesSample {

	private SitesResource api = new br.com.cNova.api.resources.v2.sites.Sites(Hosts.SANDBOX, Tokens.client_id,
			Tokens.access_token);
	
	private List<Sites> getSites() throws ServiceException {
		return api.getSites();
	}
	
	public static void main(String[] args) {
		try {

			SitesSample sitesSample = new SitesSample();
			
			List<Sites> list = sitesSample.getSites();
			
			if (list != null && list.size() > 0) {
				for (Sites site : list) {
					System.out.println("================ SITE ===================");
					System.out.println(site.getId());
					System.out.println(site.getName());
					System.out.println(site.getMnemonic());
					System.out.println(site.getUrl());
					System.out.println("================ SITE ===================");
				}
			}
			
			
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}
}
