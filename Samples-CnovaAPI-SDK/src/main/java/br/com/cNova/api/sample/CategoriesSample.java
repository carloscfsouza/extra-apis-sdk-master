package br.com.cNova.api.sample;

import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.categories.Category;
import br.com.cNova.api.resources.v2.categories.Categories;
import br.com.cNova.api.resources.v2.categories.CategoriesResource;
import br.com.cNova.api.sample.utils.JSONPrinterUtil;
import br.com.cNova.api.sample.utils.Tokens;

public class CategoriesSample {

	private CategoriesResource api = new Categories(Hosts.SANDBOX,
			Tokens.client_id, Tokens.access_token);

	public void consultarCategorias() throws ServiceException {
		System.out.println("Consultar categorias:");
		List<Category> categories = api.getCategories(0, 10);
		System.out.println(categories.size() + " - "
				+ JSONPrinterUtil.print(categories));
	}

	public void consultarCategoria(String levelId) throws ServiceException {
		System.out.println("Consultar categoria por id");
		Category category = api.getCategory(levelId);
		System.out.println(JSONPrinterUtil.print(category));
	}

	public static void main(String[] args) {
		CategoriesSample categoriesSample = new CategoriesSample();

		try {
			categoriesSample.consultarCategorias();
			//categoriesSample.consultarCategoria("32178");
		} catch (ServiceException e) {
			e.printStackTrace();
		}

	}

}
