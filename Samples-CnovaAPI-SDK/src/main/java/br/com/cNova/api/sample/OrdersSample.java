package br.com.cNova.api.sample;

import java.io.File;
import java.util.List;

import br.com.cNova.api.core.Hosts;
import br.com.cNova.api.core.exception.ServiceException;
import br.com.cNova.api.pojo.v2.orders.ItemTracking;
import br.com.cNova.api.pojo.v2.orders.Order;
import br.com.cNova.api.pojo.v2.orders.Tracking;
import br.com.cNova.api.pojo.v2.orders.sandbox.OrderLoad;
import br.com.cNova.api.sample.utils.JSONPrinterUtil;
import br.com.cNova.api.sample.utils.Tokens;

public class OrdersSample {

	private br.com.cNova.api.resources.v2.orders.OrdersResource ordersAPI = 
			new br.com.cNova.api.resources.v2.orders.Orders(Hosts.SANDBOX,
			Tokens.client_id, Tokens.access_token);
	
	 public void consultarPedido(String orderId) throws ServiceException {
	        System.out.println(JSONPrinterUtil.print(this.ordersAPI
	                .getOrder(orderId)));
	    }
	
	public void consultarItem(String orderId, String orderItemId)
            throws ServiceException {
        System.out.println(JSONPrinterUtil.print(this.ordersAPI.getOrderItem(
                orderId, orderItemId)));
    }
	
	public void consultarPedidosNovos(String offset, String limit, String purchasedAt,
			String customerName, String customerDocumentNumber) throws ServiceException {
        
		List<Order> orders = this.ordersAPI.getNewOrders(offset, limit, purchasedAt, customerName, customerDocumentNumber);
        
		System.out.println(orders.size() + " pedidos novos: "
                + JSONPrinterUtil.print(orders));
    }
	
	public void consultarPedidosAprovados(String offset, String limit, String approvedAt,
			String customerName, String customerDocumentNumber) throws ServiceException {
        
		List<Order> orders = this.ordersAPI.getApprovedOrders(offset, limit, approvedAt, customerName, customerDocumentNumber);
        
		System.out.println(orders.size() + " pedidos aprovados: "
                + JSONPrinterUtil.print(orders));
    }
	
	public void consultarPedidosCancelados(String offset, String limit, String canceledAt,
			String customerName, String customerDocumentNumber) throws ServiceException {
       
		List<Order> orders = this.ordersAPI.getCanceledOrders(offset, limit, canceledAt, customerName, customerDocumentNumber);
        
		System.out.println(orders.size() + " pedidos cancelados: "
                + JSONPrinterUtil.print(orders));
    }
	
	public void consultarPedidosEnviados(String offset, String limit, String sentAt,
			String customerName, String customerDocumentNumber) throws ServiceException {
        
		List<Order> orders = this.ordersAPI.getSentOrders(offset, limit, sentAt, customerName, customerDocumentNumber);
        
		System.out.println(orders.size() + " pedidos enviados: "
                + JSONPrinterUtil.print(orders));
    }
	
	public void consultarPedidosEntregues(String offset, String limit, String deliveredAt,
			String customerName, String customerDocumentNumber) throws ServiceException {
        
		List<Order> orders = this.ordersAPI.geDeliveredOrders(offset, limit, deliveredAt, customerName, customerDocumentNumber);
        
		System.out.println(orders.size() + " pedidos Entregues: "
                + JSONPrinterUtil.print(orders));
    }
	
	public void carregarOrders() throws ServiceException {
		OrderLoad orderLoad = new OrderLoad();
        File loads = new File("C:\\Users\\cfarias\\workspace\\Samples-CnovaAPI-SDK-v2\\Samples-CnovaAPI-SDK-v2\\Orders_JSON_V2.json");        
        
        orderLoad.setJsonFile(loads);
        System.out.println(JSONPrinterUtil.print(ordersAPI.loadOrders(orderLoad)));
    }
	
	public void registraEnvioItens(String orderId, Tracking trackingUpdate) throws ServiceException {
		System.out.println(ordersAPI.trackingsSent(orderId, trackingUpdate));
	}
	
	public void registraEntregaItens(String orderId, Tracking trackingUpdate) throws ServiceException {
		System.out.println(ordersAPI.trackingsDelivered(orderId, trackingUpdate));
	}
	
	public void confirmaCancelarItens(String orderId, ItemTracking itemTrackingUpdate) throws ServiceException {
		System.out.println(ordersAPI.trackingscCancel(orderId, itemTrackingUpdate));
	}
	
	public void trocaEnvioItens(String orderId, Tracking trackingUpdate) throws ServiceException {
		System.out.println(ordersAPI.trackingsExchange(orderId, trackingUpdate));
	}
	
	public void retornaItens(String orderId, Tracking trackingUpdate) throws ServiceException {
		System.out.println(ordersAPI.trackingsReturn(orderId, trackingUpdate));
	}
	
    public static void main(String[] args) {
        OrdersSample ordersSample = new OrdersSample();
        try {

        	ordersSample.consultarPedido("");

        } catch (ServiceException e) {
        }
    }

}
